package org.assigment.vendingmachine.machine.internal;

import org.assigment.vendingmachine.change.ChangeMachine;
import org.assigment.vendingmachine.change.exception.InsufficientChangeException;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.exception.InvalidCoinException;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.exception.SoldOutException;
import org.assigment.vendingmachine.machine.Delivery;
import org.assigment.vendingmachine.machine.VendingMachine;
import org.assigment.vendingmachine.machine.exception.AmountNotSatisfiedException;
import org.assigment.vendingmachine.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;


/**
 * A straightforward implementation of {@link VendingMachine}
 */
public class SimpleVendingMachine implements VendingMachine {

    private static final Logger logger = LoggerFactory.getLogger(SimpleVendingMachine.class);

    /**
     * Inventory manager
     */
    private Inventory<Product> inventory;

    /**
     * Change machine
     */
    private ChangeMachine changeMachine;

    /**
     * Amount of coins inserted by the user
     */
    private BigDecimal amount;

    public SimpleVendingMachine(Inventory<Product> inventory, ChangeMachine changeMachine) {

        this.inventory = inventory;
        this.changeMachine = changeMachine;
        this.amount = BigDecimal.ZERO;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Delivery select(Product product) {

        logger.debug("selected {}", product);

        if (!inventory.isAvailable(product)) {
            logger.warn("the {} is sold out", product);
            throw new SoldOutException();
        }

        Product productToDeliver = inventory.peek(product);
        if (amount.compareTo(productToDeliver.price()) < 0) {
            logger.debug("the amount {} is insufficient for {}", amount, productToDeliver);
            throw new AmountNotSatisfiedException();
        }

        BigDecimal change = amount.subtract(product.price());
        logger.debug("change to give back {}", change);
        if (!changeMachine.isChangeAvailable(change)) {
            logger.warn("change is unavailable");
            throw new InsufficientChangeException();
        }

        Delivery delivery = new Delivery(inventory.remove(product), changeMachine.change(change));
        amount = BigDecimal.ZERO;

        return delivery;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Delivery> insert(Coin coin) {

        logger.debug("inserted {}", coin);

        try {

            changeMachine.add(coin);
            amount = amount.add(coin.value());
            return Optional.empty();

        } catch (InvalidCoinException e) {
            logger.warn("inserted {} is invalid", coin);
            return Optional.of(new Delivery(Arrays.asList(coin)));
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Delivery> cancel() {

        logger.debug("try to cancel amount {}", amount);

        if (amount.compareTo(BigDecimal.ZERO) == 0) {
            return Optional.empty();
        }

        // Always the amount is >= 0
        Delivery delivery = new Delivery(changeMachine.change(amount));
        amount = BigDecimal.ZERO;

        logger.debug("canceled and given back {}", delivery.coins());

        return Optional.of(delivery);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.inventory.clear();
        this.changeMachine.clear();
        this.amount = BigDecimal.ZERO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Inventory<Product> inventory() {
        return inventory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ChangeMachine changeMachine() {
        return changeMachine;
    }

}
