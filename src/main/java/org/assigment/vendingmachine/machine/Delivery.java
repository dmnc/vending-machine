package org.assigment.vendingmachine.machine;

import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.product.Product;

import java.util.List;

/**
 * Pair of {@link Product} and {@link List<Coin>}
 */
public class Delivery {

    /**
     * Selected product
     */
    private Product product;

    /**
     * User's change
     */
    private List<Coin> coins;

    /**
     * Creates an instance of {@link Delivery} using the given {@link List<Coin>}
     *
     * @param coins list of coins to deliver
     */
    public Delivery(List<Coin> coins) {
        this(null, coins);
    }

    /**
     * All in constructor
     *
     * @param product a product to deliver
     * @param coins   list of coins to deliver
     */
    public Delivery(Product product, List<Coin> coins) {
        this.product = product;
        this.coins = coins;
    }

    /**
     * @see #product
     */
    public Product product() {
        return product;
    }

    /**
     * @see #coins
     */
    public List<Coin> coins() {
        return coins;
    }

}
