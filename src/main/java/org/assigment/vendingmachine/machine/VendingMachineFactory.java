package org.assigment.vendingmachine.machine;

import org.assigment.vendingmachine.change.ChangeMachine;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.machine.internal.SimpleVendingMachine;
import org.assigment.vendingmachine.product.Product;

/**
 * Factory to create {@link VendingMachine}
 */
public final class VendingMachineFactory {

    public enum Type {
        SIMPLE
    }

    private VendingMachineFactory() {
    }

    /**
     * Creates an instance of {@link VendingMachine} of the given type
     *
     * @param type          type of {@link VendingMachine}
     * @param inventory     inventory to manage the stock of the products
     * @param changeMachine change machine
     * @return an instance of {@link VendingMachine}
     */
    public static VendingMachine create(Type type, Inventory<Product> inventory, ChangeMachine changeMachine) {
        switch (type) {
            case SIMPLE:
            default:
                return new SimpleVendingMachine(inventory, changeMachine);
        }
    }

}
