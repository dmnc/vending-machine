package org.assigment.vendingmachine.machine.exception;

/**
 * Raised when an user select a product and the inserted amount does not satisfy the price of the product
 */
public class AmountNotSatisfiedException extends RuntimeException {
}
