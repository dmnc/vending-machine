package org.assigment.vendingmachine.machine;

import org.assigment.vendingmachine.change.ChangeMachine;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.product.Product;

import java.util.Optional;

/**
 * Vending machine contract
 */
public interface VendingMachine {

    /**
     * Delivers a product if there is enough money to buy the product
     *
     * @param product product to buy
     * @return (product, coins)
     */
    Delivery select(Product product);

    /**
     * Inserts a coin
     *
     * @param coin coin to insert
     * @return the given coin if it is not allowed
     */
    Optional<Delivery> insert(Coin coin);

    /**
     * Gives back the money
     *
     * @return (coins)
     */
    Optional<Delivery> cancel();

    /**
     * Reset the machine
     */
    void reset();

    /**
     * Retrieves product inventory
     */
    Inventory<Product> inventory();


    /**
     * Retrieves change machine
     */
    ChangeMachine changeMachine();

}
