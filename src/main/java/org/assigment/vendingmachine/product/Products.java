package org.assigment.vendingmachine.product;

import java.math.BigDecimal;

/**
 * Factory to create the available products
 */
public final class Products {

    private Products() {
    }

    /**
     * Creates a coke product
     */
    public static Product coke() {
        return new Product("coke", new BigDecimal("1.50"));
    }

    /**
     * Creates a pepsi product
     */
    public static Product pepsi() {
        return new Product("pepsi", new BigDecimal("1.45"));
    }

    /**
     * Creates a water product
     */
    public static Product water() {
        return new Product("water", new BigDecimal("0.90"));
    }

}
