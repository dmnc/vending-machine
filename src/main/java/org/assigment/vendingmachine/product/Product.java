package org.assigment.vendingmachine.product;

import java.math.BigDecimal;

/**
 * Product of this vending machine
 */
public class Product {

    /**
     * Type of this product
     */
    private String type;

    /**
     * Value of this product
     */
    private BigDecimal price;

    public Product(String type, BigDecimal price) {
        this.type = type;
        this.price = price;
    }

    /**
     * @see #price
     */
    public BigDecimal price() {
        return price;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (type != null ? !type.equals(product.type) : product.type != null) return false;
        return price != null ? price.equals(product.price) : product.price == null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Product{" +
                "type='" + type + '\'' +
                ", price=" + price +
                '}';
    }

}
