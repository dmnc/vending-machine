package org.assigment.vendingmachine.coin;

import java.math.BigDecimal;

/**
 * Factory to create coins
 */
public final class Coins {

    private Coins() {

    }

    /**
     * Creates an instance of {@link Coin} of 2 euros
     */
    public static Coin twoEuros() {
        return new Coin("2€", new BigDecimal("2"));
    }

    /**
     * Creates an instance of {@link Coin} of 1 euros
     */
    public static Coin oneEuro() {
        return new Coin("1€", new BigDecimal("1"));
    }

    /**
     * Creates an instance of {@link Coin} of 0.50 euros
     */
    public static Coin fiftyCentsOfEuro() {
        return new Coin("0.50€", new BigDecimal("0.50"));
    }

    /**
     * Creates an instance of {@link Coin} of 0.20 euros
     */
    public static Coin twentyCentsOfEuro() {
        return new Coin("0.20€", new BigDecimal("0.20"));
    }

    /**
     * Creates an instance of {@link Coin} of 0.10 euros
     */
    public static Coin tenCentsOfEuro() {
        return new Coin("0.10€", new BigDecimal("0.10"));
    }

    /**
     * Creates an instance of {@link Coin} of 0.05 euros
     */
    public static Coin fiveCentsOfEuro() {
        return new Coin("0.05€", new BigDecimal("0.05"));
    }

}
