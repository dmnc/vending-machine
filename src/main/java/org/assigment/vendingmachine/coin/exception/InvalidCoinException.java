package org.assigment.vendingmachine.coin.exception;

/**
 * Raised when somebody try to use an not allowed coin
 */
public class InvalidCoinException extends RuntimeException {
}
