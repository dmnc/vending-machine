package org.assigment.vendingmachine.coin;

import java.math.BigDecimal;

/**
 * Coin
 * <p>
 * 0.05€, 0.10€, 0.20€, 0.50€, 1€, 2€
 */
public class Coin {

    /**
     * Type of coin
     */
    private String type;

    /**
     * Value of this coin
     */
    private BigDecimal value;

    /**
     * All in constructor
     *
     * @param type  type of the coin
     * @param value value of the coin
     */
    public Coin(String type, BigDecimal value) {
        this.type = type;
        this.value = value;
    }

    /**
     * @see #value
     */
    public BigDecimal value() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coin coin = (Coin) o;

        if (type != null ? !type.equals(coin.type) : coin.type != null) return false;
        return value != null ? value.equals(coin.value) : coin.value == null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Coin{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }

}
