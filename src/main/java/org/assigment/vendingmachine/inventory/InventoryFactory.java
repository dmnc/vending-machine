package org.assigment.vendingmachine.inventory;

import org.assigment.vendingmachine.inventory.internal.SimpleInventoryManager;

import java.util.List;

/**
 * Factory to create inventory managers
 */
public final class InventoryFactory {

    public enum Type {
        SIMPLE
    }

    private InventoryFactory() {
    }

    /**
     * Creates an instance of {@link Inventory}
     *
     * @param type    type of inventory
     * @param allowed items allowed
     * @return an instance
     */
    public static <T> Inventory<T> create(Type type, List<T> allowed) {
        switch (type) {
            case SIMPLE:
            default:
                return new SimpleInventoryManager<>(allowed);
        }
    }

}
