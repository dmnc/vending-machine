package org.assigment.vendingmachine.inventory.exception;

/**
 * Raised when a item is sold out
 */
public class SoldOutException extends RuntimeException {
}
