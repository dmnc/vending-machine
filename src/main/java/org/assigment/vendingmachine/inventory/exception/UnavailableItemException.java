package org.assigment.vendingmachine.inventory.exception;

/**
 * Raised when a item is unavailable. Not allowed in this inventory
 */
public class UnavailableItemException extends RuntimeException {
}
