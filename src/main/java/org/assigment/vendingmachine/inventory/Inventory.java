package org.assigment.vendingmachine.inventory;

import org.assigment.vendingmachine.inventory.exception.SoldOutException;
import org.assigment.vendingmachine.inventory.exception.UnavailableItemException;

import java.util.Map;

/**
 * Manages an inventory
 */
public interface Inventory<T> {

    /**
     * Adds a item
     *
     * @param item item to be added
     * @throws UnavailableItemException if the item is not allowed
     */
    void add(T item);

    /**
     * Retrieves an item of the give type
     *
     * @param item type to remove
     * @return an item
     * @throws UnavailableItemException if the item is not allowed
     * @throws SoldOutException         if the item is sold out
     */
    T remove(T item);

    /**
     * Retrieves, but does not remove, an item of the give type
     *
     * @param item type to remove
     * @return an item
     * @throws UnavailableItemException if the item is not allowed
     * @throws SoldOutException         if the item is sold out
     */
    T peek(T item);

    /**
     * Status of the items
     *
     * @return current availability of the current items
     */
    Map<T, Integer> status();

    /**
     * Checks if an item is available in the inventory
     *
     * @param item item to check
     * @return true if the item is available, otherwise false
     */
    Boolean isAvailable(T item);

    /**
     * Cleans up the inventory
     */
    void clear();

}
