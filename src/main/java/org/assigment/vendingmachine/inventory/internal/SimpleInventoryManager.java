package org.assigment.vendingmachine.inventory.internal;

import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.exception.SoldOutException;
import org.assigment.vendingmachine.inventory.exception.UnavailableItemException;

import java.util.*;

import static java.util.Collections.emptyList;

/**
 * Manage a stock of items. This implementation has no capacity restrictions
 *
 * @since 1.0.0
 */
public class SimpleInventoryManager<T> implements Inventory<T> {

    /**
     * Holds the current inventory
     */
    private Map<T, Queue<T>> inventory;
    /**
     *
     */
    private Map<T, Integer> counter;

    public SimpleInventoryManager(List<T> allowed) {
        inventory = new HashMap<>();
        counter = new HashMap<>();

        allowed.forEach(item -> {
            inventory.put(item, new ArrayDeque<>(emptyList()));
            counter.put(item, 0);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(T item) {

        if (!inventory.containsKey(item)) {
            throw new UnavailableItemException();
        }

        inventory.compute(item, (k, v) -> {
            v.offer(item);
            return v;
        });

        counter.compute(item, (k, v) -> v + 1);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T remove(T item) {

        check(item);

        counter.computeIfPresent(item, (k, v) -> v - 1);
        return inventory.get(item).poll();

    }

    /**
     * Checks if the given type of product exists
     *
     * @param item item
     */
    private void check(T item) {

        if (!inventory.containsKey(item)) {
            throw new UnavailableItemException();
        }

        if (inventory.get(item).isEmpty()) {
            throw new SoldOutException();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peek(T item) {

        check(item);

        return inventory.get(item).peek();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<T, Integer> status() {

        return new HashMap<>(counter);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean isAvailable(T item) {

        if (!inventory.containsKey(item) || inventory.get(item).isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {

        inventory.entrySet().forEach(entry -> entry.getValue().clear());

        counter.entrySet().forEach(entry -> counter.compute(entry.getKey(), (k, v) -> 0));

    }

}
