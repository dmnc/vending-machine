package org.assigment.vendingmachine.change.exception;

import org.assigment.vendingmachine.change.ChangeMachine;

/**
 * Raised when {@link ChangeMachine} does not have sufficient coins to give back the change
 */
public class InsufficientChangeException extends RuntimeException {
}
