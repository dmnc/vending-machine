package org.assigment.vendingmachine.change;

import org.assigment.vendingmachine.coin.Coin;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

/**
 * Change strategy used in {@link ChangeMachine} to calculate the change
 */
public interface ChangeStrategy {

    /**
     * Tries to calculate the changes of the given amount
     *
     * @param availability available coins
     * @param amount       amount to calculate the change
     * @return maybe change
     */
    Optional<Map<Coin, Integer>> change(Map<Coin, Integer> availability, BigDecimal amount);

}
