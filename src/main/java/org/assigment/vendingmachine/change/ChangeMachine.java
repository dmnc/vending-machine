package org.assigment.vendingmachine.change;

import org.assigment.vendingmachine.coin.Coin;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Manage change
 */
public interface ChangeMachine {

    /**
     * Adds a coin
     *
     * @param coin coin to add
     */
    void add(Coin coin);

    /**
     * Removes a coin
     *
     * @param coin type of the coin to remove
     * @return maybe a coin o the given type
     */
    Optional<Coin> remove(Coin coin);

    /**
     * Removes an amount of coins
     *
     * @param coin   type of the coin
     * @param amount amount to remove
     * @return removed item
     */
    List<Coin> remove(Coin coin, Integer amount);

    /**
     * Checks if there are enough coins to give back the change
     *
     * @param amount change to give back
     * @return true if there are enough coins, otherwise false
     */
    Boolean isChangeAvailable(BigDecimal amount);

    /**
     * Change
     *
     * @param amount change to give back
     * @return change
     */
    List<Coin> change(BigDecimal amount);

    /**
     * Gives the current status
     *
     * @return coin status
     */
    Map<Coin, Integer> status();

    /**
     * Clear up the machine
     */
    void clear();

}
