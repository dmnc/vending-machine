package org.assigment.vendingmachine.change.internal;

import org.assigment.vendingmachine.change.ChangeMachine;
import org.assigment.vendingmachine.change.ChangeStrategy;
import org.assigment.vendingmachine.change.exception.InsufficientChangeException;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.exception.InvalidCoinException;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.exception.UnavailableItemException;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * A straightforward implementation of {@link ChangeMachine}
 */
public class SimpleChangeMachine implements ChangeMachine {

    /**
     * Strategy to calculate the change to give back
     */
    private ChangeStrategy strategy;

    /**
     * Coin inventory
     */
    private Inventory<Coin> inventory;


    public SimpleChangeMachine(Inventory<Coin> inventory, ChangeStrategy strategy) {

        this.inventory = inventory;
        this.strategy = strategy;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(Coin coin) {

        try {
            inventory.add(coin);
        } catch (UnavailableItemException e) {
            throw new InvalidCoinException();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Coin> remove(Coin coin) {

        return inventory.isAvailable(coin) ? Optional.of(inventory.remove(coin)) : Optional.empty();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Coin> remove(Coin coin, Integer amount) {

        return IntStream.range(0, amount).mapToObj(i -> remove(coin)).filter(Optional::isPresent).map(Optional::get).collect(toList());
        
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean isChangeAvailable(BigDecimal amount) {
        return strategy.change(inventory.status(), amount).isPresent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Coin> change(BigDecimal amount) {

        if (amount.compareTo(BigDecimal.ZERO) == 0 || amount.signum() == -1) {
            return Collections.emptyList();
        }

        Optional<Map<Coin, Integer>> change = strategy.change(inventory.status(), amount);

        if (change.isPresent()) {
            return change.map(x -> x.entrySet()
                    .stream()
                    .flatMap(entry -> remove(entry.getKey(), entry.getValue()).stream())
            ).get().collect(toList());
        } else {
            throw new InsufficientChangeException();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Coin, Integer> status() {

        return inventory.status();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {

        inventory.clear();

    }

}
