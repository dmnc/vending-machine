package org.assigment.vendingmachine.change.internal;

import org.assigment.vendingmachine.coin.Coin;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Accumulator class to help to calculate change
 */
class ChangeAccumulator {

    /**
     * Pending amount to calculate the change
     */
    private BigDecimal excess;

    /**
     * Ongoing change coins
     */
    private Map<Coin, Integer> coins;

    ChangeAccumulator(BigDecimal amount) {
        this.excess = amount;
        this.coins = new HashMap<>();
    }

    /**
     * Checks if all change is covered
     *
     * @return true if all change is covered, otherwise false
     */
    Boolean isExcess() {
        return excess.compareTo(BigDecimal.ZERO) > 0;
    }

    /**
     * @see #excess
     */
    BigDecimal getExcess() {
        return excess;
    }

    /**
     * Removes the amount that lacks to cover the change
     *
     * @param amount amount
     */
    void subtract(BigDecimal amount) {
        excess = excess.subtract(amount);
    }

    /**
     * @see #coins
     */
    Map<Coin, Integer> getCoins() {
        return coins;
    }

    /**
     * Adds a {@link Coin} to give back
     *
     * @param coin   a {@link Coin}
     * @param amount amount of {@link Coin}
     */
    void addCoin(Coin coin, Integer amount) {
        coins.put(coin, amount);
    }

}
