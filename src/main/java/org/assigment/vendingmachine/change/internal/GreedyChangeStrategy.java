package org.assigment.vendingmachine.change.internal;

import org.assigment.vendingmachine.change.ChangeStrategy;
import org.assigment.vendingmachine.coin.Coin;

import java.math.BigDecimal;
import java.util.*;

/**
 * Implementation of {@link ChangeStrategy} that uses a greedy way to calculate the change
 */
public class GreedyChangeStrategy implements ChangeStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Map<Coin, Integer>> change(Map<Coin, Integer> availability, BigDecimal amount) {

        if (amount.compareTo(BigDecimal.ZERO) == 0 || amount.signum() == -1) {
            return Optional.of(Collections.emptyMap());
        }

        Comparator<Coin> comparator = (o1, o2) -> o1.value().compareTo(o2.value());
        Map<Coin, Integer> counter = new TreeMap<>(comparator.reversed());
        counter.putAll(availability);

        ChangeAccumulator collect = counter
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 0)
                .collect(() -> new ChangeAccumulator(amount), (acc, entry) -> {

                    if (acc.isExcess() && acc.getExcess().compareTo(entry.getKey().value()) >= 0) {

                        // Available
                        BigDecimal num = acc.getExcess().divideToIntegralValue(entry.getKey().value()).min(new BigDecimal(entry.getValue
                                ()));

                        if (num.compareTo(BigDecimal.ZERO) >= 0) {
                            acc.addCoin(entry.getKey(), num.intValue());
                            acc.subtract(entry.getKey().value().multiply(num));
                        }
                    }
                }, (acc1, acc2) -> { // we do not use parallel execution
                });

        return collect.isExcess() || collect.getCoins().isEmpty() ? Optional.empty() : Optional.of(collect.getCoins());

    }
}

