package org.assigment.vendingmachine.change;

import org.assigment.vendingmachine.change.internal.GreedyChangeStrategy;
import org.assigment.vendingmachine.change.internal.SimpleChangeMachine;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.inventory.Inventory;

/**
 * Factory to create {@link ChangeMachine}
 */
public final class ChangeMachineFactory {

    public enum Type {
        SIMPLE
    }

    private ChangeMachineFactory() {
    }

    /**
     * Creates an instance of {@link ChangeMachine}
     *
     * @param type      type of {@link ChangeMachine}
     * @param inventory inventory to manage the coins
     * @return an instance of {@link ChangeMachine}
     */
    public static ChangeMachine create(Type type, Inventory<Coin> inventory) {
        switch (type) {
            case SIMPLE:
            default:
                return new SimpleChangeMachine(inventory, new GreedyChangeStrategy());
        }
    }

}
