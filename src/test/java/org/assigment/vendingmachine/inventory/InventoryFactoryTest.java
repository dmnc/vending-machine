package org.assigment.vendingmachine.inventory;

import org.assigment.vendingmachine.inventory.internal.SimpleInventoryManager;
import org.assigment.vendingmachine.product.Product;
import org.assigment.vendingmachine.product.Products;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * Validates {@link InventoryFactory}
 */
public class InventoryFactoryTest {

    @Test
    public void simpleType() {

        // Exercise
        Inventory<Product> inventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Products.coke(),
                        Products.pepsi(),
                        Products.water()
                ));

        // Verify
        assertThat(inventory, instanceOf(SimpleInventoryManager.class));

    }

}
