package org.assigment.vendingmachine.inventory.internal;

import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.InventoryFactory;
import org.assigment.vendingmachine.inventory.exception.SoldOutException;
import org.assigment.vendingmachine.inventory.exception.UnavailableItemException;
import org.assigment.vendingmachine.product.Product;
import org.assigment.vendingmachine.product.Products;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.assigment.vendingmachine.product.Products.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Validates {@link SimpleInventoryManager}
 * <p>
 * Coke(1.50€), Pepsi(1.45€), Water(0.90€)
 */
public class SimpleInventoryManagerTest {

    @Test
    public void availableCoinsAreInitialized() {

        // Setup
        Inventory<Product> inventory = generate();

        // Exercise
        Map<Product, Integer> status = inventory.status();

        // Verify
        assertThat(status, hasEntry(is(coke()), is(0)));
        assertThat(status, hasEntry(is(pepsi()), is(0)));
        assertThat(status, hasEntry(is(water()), is(0)));

    }

    @Test
    public void checkAnItem() {

        // Setup
        Inventory<Product> inventory = generate();
        inventory.add(coke());

        // Exercise
        Integer amountBefore = inventory.status().get(coke());
        inventory.peek(coke());
        Integer amountAfter = inventory.status().get(coke());

        // Verify
        assertThat(amountBefore, is(amountAfter));

    }

    @Test
    public void checkIfAProductIsAvailable() {

        // Setup
        Inventory<Product> inventory = generate();
        Product coke = coke();
        Product pepsi = pepsi();
        Product water = water();

        inventory.add(coke);
        inventory.add(water);
        inventory.add(pepsi);

        // Exercise and Verify
        assertThat(inventory.isAvailable(coke), is(true));
        assertThat(inventory.isAvailable(water), is(true));
        assertThat(inventory.isAvailable(pepsi), is(true));

        Map<Product, Integer> status = inventory.status();
        assertThat(status, hasEntry(is(coke), is(1)));
        assertThat(status, hasEntry(is(water), is(1)));
        assertThat(status, hasEntry(is(pepsi), is(1)));

    }

    @Test
    public void removeAnAvailableProduct() {

        // Setup
        Inventory<Product> inventory = generate();
        Product coke = coke();
        Product pepsi = pepsi();
        Product water = water();

        inventory.add(coke);
        inventory.add(water);
        inventory.add(pepsi);

        // Exercise and Verify
        Product pepsiProduct = inventory.remove(pepsi);
        assertThat(pepsiProduct, is(notNullValue()));
        assertThat(inventory.isAvailable(pepsi), is(false));

        Product cokeProduct = inventory.remove(coke);
        assertThat(cokeProduct, is(notNullValue()));
        assertThat(inventory.isAvailable(coke), is(false));

        Product waterProduct = inventory.remove(water);
        assertThat(waterProduct, is(notNullValue()));
        assertThat(inventory.isAvailable(water), is(false));

    }

    @Test(expected = UnavailableItemException.class)
    public void tryToAddANotValidProduct() {

        // Setup
        Inventory<Product> inventory = generate();

        // Exercise
        inventory.add(new Product("????", new BigDecimal("33")));

        // Verify ... check expected

    }

    @Test(expected = UnavailableItemException.class)
    public void tryToRemoveNotValidProduct() {

        // Setup
        Inventory<Product> inventory = generate();

        // Exercise
        inventory.remove(new Product("????", new BigDecimal("33")));

        // Verify ... check expected

    }

    @Test(expected = SoldOutException.class)
    public void soldOutProduct() {

        // Setup
        Inventory<Product> inventory = generate();
        Product coke = coke();
        inventory.add(coke);
        inventory.add(coke());
        inventory.add(coke());

        // Exercise
        inventory.remove(coke);
        inventory.remove(coke);
        inventory.remove(coke);
        inventory.remove(coke);

        // Verify ... check expected

    }

    @Test
    public void afterAClearTheInventoryShouldBeClean() {

        // Setup
        Inventory<Product> inventory = generate();
        inventory.add(coke());
        inventory.add(coke());
        inventory.add(water());
        inventory.add(water());

        // Exercise
        inventory.clear();

        // Verify
        assertThat(inventory.isAvailable(coke()), is(false));
        assertThat(inventory.isAvailable(water()), is(false));
        assertThat(inventory.status(), hasEntry(is(coke()), is(0)));
        assertThat(inventory.status(), hasEntry(is(pepsi()), is(0)));
        assertThat(inventory.status(), hasEntry(is(water()), is(0)));

    }

    /**
     * Helper class to generate inventory
     */
    public Inventory<Product> generate() {

        return InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Products.coke(),
                        Products.pepsi(),
                        Products.water()
                ));

    }

}
