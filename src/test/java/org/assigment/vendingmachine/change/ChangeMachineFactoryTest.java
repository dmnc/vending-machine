package org.assigment.vendingmachine.change;

import org.assigment.vendingmachine.change.internal.SimpleChangeMachine;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.Coins;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.InventoryFactory;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Validates {@link ChangeMachineFactory}
 */
public class ChangeMachineFactoryTest {

    @Test
    public void simpleType() {

        // Setup
        Inventory<Coin> coinInventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Coins.twoEuros(),
                        Coins.oneEuro(),
                        Coins.fiftyCentsOfEuro(),
                        Coins.twentyCentsOfEuro(),
                        Coins.tenCentsOfEuro(),
                        Coins.fiveCentsOfEuro()
                ));

        // Exercise
        ChangeMachine machine = ChangeMachineFactory.create(ChangeMachineFactory.Type.SIMPLE, coinInventory);

        // Verify
        assertThat(machine, instanceOf(SimpleChangeMachine.class));

        assertThat(machine.status(), hasEntry(is(Coins.twoEuros()), is(0)));
        assertThat(machine.status(), hasEntry(is(Coins.oneEuro()), is(0)));
        assertThat(machine.status(), hasEntry(is(Coins.fiftyCentsOfEuro()), is(0)));
        assertThat(machine.status(), hasEntry(is(Coins.twentyCentsOfEuro()), is(0)));
        assertThat(machine.status(), hasEntry(is(Coins.tenCentsOfEuro()), is(0)));
        assertThat(machine.status(), hasEntry(is(Coins.fiveCentsOfEuro()), is(0)));

    }
}
