package org.assigment.vendingmachine.change.internal;

import org.assigment.vendingmachine.change.ChangeStrategy;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.Coins;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Validates {@link GreedyChangeStrategy}
 * <p>
 * TODO we should use a random generator test too
 */
public class GreedyChangeStrategyTest {

    @Test
    public void emptyAvailableCoins() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("1"));

        // Verify
        assertThat(maybeChange.isPresent(), is(false));

    }

    @Test
    public void negativeAmount() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();
        availability.put(Coins.tenCentsOfEuro(), 3);
        availability.put(Coins.twoEuros(), 3);
        availability.put(Coins.oneEuro(), 4);

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("-0.90"));

        // Verify
        assertThat(maybeChange.isPresent(), is(true));
        assertThat(maybeChange.get(), is(anEmptyMap()));

    }

    @Test
    public void zeroAmount() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();
        availability.put(Coins.tenCentsOfEuro(), 3);
        availability.put(Coins.twoEuros(), 3);
        availability.put(Coins.oneEuro(), 4);

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("0.0"));

        // Verify
        assertThat(maybeChange.isPresent(), is(true));
        assertThat(maybeChange.get(), is(anEmptyMap()));

    }

    @Test
    public void insufficientCoinsOf5Cents() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();
        availability.put(Coins.twoEuros(), 1);
        availability.put(Coins.oneEuro(), 1);
        availability.put(Coins.fiftyCentsOfEuro(), 4);
        availability.put(Coins.twentyCentsOfEuro(), 4);
        availability.put(Coins.tenCentsOfEuro(), 4);
        availability.put(Coins.fiveCentsOfEuro(), 0);

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("0.05"));

        // Verify
        assertThat(maybeChange.isPresent(), is(false));

    }

    @Test
    public void insufficientCoinsOf10Cents() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();
        availability.put(Coins.twoEuros(), 0);
        availability.put(Coins.oneEuro(), 0);
        availability.put(Coins.fiftyCentsOfEuro(), 0);
        availability.put(Coins.twentyCentsOfEuro(), 1);
        availability.put(Coins.tenCentsOfEuro(), 0);
        availability.put(Coins.fiveCentsOfEuro(), 4);

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("0.50"));

        // Verify
        assertThat(maybeChange.isPresent(), is(false));

    }

    @Test
    public void exhaustingAllCoins() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();
        availability.put(Coins.twoEuros(), 1);
        availability.put(Coins.oneEuro(), 1);
        availability.put(Coins.fiftyCentsOfEuro(), 4);
        availability.put(Coins.twentyCentsOfEuro(), 4);
        availability.put(Coins.tenCentsOfEuro(), 4);
        availability.put(Coins.fiveCentsOfEuro(), 4);

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("900"));

        // Verify
        assertThat(maybeChange.isPresent(), is(false));

    }

    @Test
    public void hasChange() {

        // Setup
        Map<Coin, Integer> availability = new HashMap<>();
        availability.put(Coins.twoEuros(), 1);
        availability.put(Coins.oneEuro(), 1);
        availability.put(Coins.fiftyCentsOfEuro(), 4);
        availability.put(Coins.twentyCentsOfEuro(), 4);
        availability.put(Coins.tenCentsOfEuro(), 4);
        availability.put(Coins.fiveCentsOfEuro(), 4);

        ChangeStrategy changeStrategy = new GreedyChangeStrategy();

        // Exercise
        Optional<Map<Coin, Integer>> maybeChange = changeStrategy.change(availability, new BigDecimal("0.90"));

        // Verify
        assertThat(maybeChange.isPresent(), is(true));

        Map<Coin, Integer> change = maybeChange.get();
        assertThat(change.entrySet(), hasSize(2));

        assertThat(change, hasEntry(is(Coins.fiftyCentsOfEuro()), is(1)));
        assertThat(change, hasEntry(is(Coins.twentyCentsOfEuro()), is(2)));

    }

}
