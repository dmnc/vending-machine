package org.assigment.vendingmachine.change.internal;

import org.assigment.vendingmachine.change.ChangeStrategy;
import org.assigment.vendingmachine.change.exception.InsufficientChangeException;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.Coins;
import org.assigment.vendingmachine.coin.exception.InvalidCoinException;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.InventoryFactory;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;
import static org.assigment.vendingmachine.coin.Coins.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Validates {@link SimpleChangeMachine}
 */
public class SimpleChangeMachineTest {

    @Test
    public void availableCoinsAreInitialized() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise
        Map<Coin, Integer> status = machine.status();

        // Verify
        assertThat(status, hasEntry(is(twoEuros()), is(0)));
        assertThat(status, hasEntry(is(oneEuro()), is(0)));
        assertThat(status, hasEntry(is(fiftyCentsOfEuro()), is(0)));
        assertThat(status, hasEntry(is(twentyCentsOfEuro()), is(0)));
        assertThat(status, hasEntry(is(Coins.tenCentsOfEuro()), is(0)));
        assertThat(status, hasEntry(is(Coins.fiveCentsOfEuro()), is(0)));

    }

    @Test(expected = InvalidCoinException.class)
    public void addInvalidCoin() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise
        machine.add(new Coin("0.01", new BigDecimal("1")));

        // Verify ... check expected

    }

    @Test
    public void addAndRemoveDoesNotMixTypes() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise
        machine.add(oneEuro());
        machine.add(twoEuros());

        Optional<Coin> removed = machine.remove(oneEuro());

        // Verify
        assertThat(removed.isPresent(), is(true));
        assertThat(removed.get(), is(oneEuro()));
    }

    @Test
    public void addsEqualsToRemoves() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise
        machine.add(oneEuro());
        machine.add(oneEuro());
        machine.add(twoEuros());
        machine.add(twoEuros());

        // Verify
        assertThat(machine.remove(oneEuro()).isPresent(), is(true));
        assertThat(machine.remove(oneEuro()).isPresent(), is(true));
        assertThat(machine.remove(oneEuro()).isPresent(), is(false));
        assertThat(machine.remove(twoEuros()).isPresent(), is(true));
        assertThat(machine.remove(twoEuros()).isPresent(), is(true));
        assertThat(machine.remove(twoEuros()).isPresent(), is(false));

        assertThat(machine.status(), hasEntry(is(oneEuro()), is(0)));
        assertThat(machine.status(), hasEntry(is(twoEuros()), is(0)));

    }

    @Test
    public void addsEqualsToBulkRemove() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise and Verify
        machine.add(oneEuro());
        machine.add(oneEuro());
        machine.add(twoEuros());
        machine.add(twoEuros());

        List<Coin> oneEuroCoins = machine.remove(oneEuro(), 3);
        assertThat(oneEuroCoins, hasSize(2));

        List<Coin> twoEurosCoins = machine.remove(twoEuros(), 2);
        assertThat(twoEurosCoins, hasSize(2));

        assertThat(machine.status(), hasEntry(is(oneEuro()), is(0)));
        assertThat(machine.status(), hasEntry(is(twoEuros()), is(0)));

    }

    @Test
    public void checkIfChangeIsAvailableAndItIsUnavailable() {

        // Setup
        ChangeStrategy changeStrategy = mock(ChangeStrategy.class);
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), changeStrategy);

        machine.add(twoEuros());
        machine.add(oneEuro());
        IntStream.range(0, 4).forEach(i -> machine.add(fiftyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(twentyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.tenCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.fiveCentsOfEuro()));

        when(changeStrategy.change(any(), any())).thenReturn(Optional.empty());

        // Exercise
        Boolean isAvailable = machine.isChangeAvailable(new BigDecimal("33"));

        // Verify
        assertThat(isAvailable, is(false));

    }

    @Test
    public void checkIfChangeIsAvailableAndItIs() {

        // Setup
        ChangeStrategy changeStrategy = mock(ChangeStrategy.class);
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), changeStrategy);

        machine.add(twoEuros());
        machine.add(oneEuro());
        IntStream.range(0, 4).forEach(i -> machine.add(fiftyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(twentyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.tenCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.fiveCentsOfEuro()));

        when(changeStrategy.change(any(), any())).thenAnswer((a) -> {
            Map<Coin, Integer> change = new HashMap<>();
            change.put(fiftyCentsOfEuro(), 1);
            return Optional.of(change);
        });

        // Exercise
        Boolean isAvailable = machine.isChangeAvailable(new BigDecimal("33"));

        // Verify
        assertThat(isAvailable, is(true));

    }

    @Test
    public void changeOfZero() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise
        List<Coin> change = machine.change(BigDecimal.ZERO);

        // Verify
        assertThat(change, is(empty()));
    }

    @Test
    public void changeOfNegativeAmount() {

        // Setup
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), null);

        // Exercise
        List<Coin> change = machine.change(new BigDecimal("-1"));

        // Verify
        assertThat(change, is(empty()));

    }

    @Test
    public void change() {

        // Setup
        ChangeStrategy changeStrategy = mock(ChangeStrategy.class);
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), changeStrategy);

        machine.add(twoEuros());
        machine.add(oneEuro());
        IntStream.range(0, 4).forEach(i -> machine.add(fiftyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(twentyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.tenCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.fiveCentsOfEuro()));

        when(changeStrategy.change(any(), any())).thenAnswer((a) -> {
            Map<Coin, Integer> change = new HashMap<>();
            change.put(fiftyCentsOfEuro(), 1);
            change.put(twentyCentsOfEuro(), 2);

            return Optional.of(change);
        });

        // Exercise
        List<Coin> change = machine.change(new BigDecimal("0.90"));

        // Verify change
        assertThat(change, hasSize(3));
        assertThat(change, hasItem(fiftyCentsOfEuro()));
        assertThat(change, hasItem(twentyCentsOfEuro()));

        // Verify status machine
        Map<Coin, Integer> status = machine.status();
        assertThat(status, hasEntry(fiftyCentsOfEuro(), 3));
        assertThat(status, hasEntry(twentyCentsOfEuro(), 2));

    }

    @Test(expected = InsufficientChangeException.class)
    public void changeOrderedButThereIsNotEnoughCoins() {

        // Setup
        ChangeStrategy changeStrategy = mock(ChangeStrategy.class);
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), changeStrategy);

        IntStream.range(0, 4).forEach(i -> machine.add(Coins.fiveCentsOfEuro()));

        when(changeStrategy.change(any(), any())).thenReturn(Optional.empty());

        // Exercise
        machine.change(new BigDecimal("0.90"));

        // Verify ... check expected

    }

    @Test
    public void clean() {

        // Setup
        ChangeStrategy changeStrategy = mock(ChangeStrategy.class);
        SimpleChangeMachine machine = new SimpleChangeMachine(generate(), changeStrategy);

        machine.add(twoEuros());
        machine.add(oneEuro());
        IntStream.range(0, 4).forEach(i -> machine.add(fiftyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(twentyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.tenCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> machine.add(Coins.fiveCentsOfEuro()));

        // Exercise
        machine.clear();

        // Verify
        Map<Coin, Integer> status = machine.status();
        assertThat(status, hasEntry(is(twoEuros()), is(0)));
        assertThat(status, hasEntry(is(oneEuro()), is(0)));
        assertThat(status, hasEntry(is(fiftyCentsOfEuro()), is(0)));
        assertThat(status, hasEntry(is(twentyCentsOfEuro()), is(0)));
        assertThat(status, hasEntry(is(tenCentsOfEuro()), is(0)));
        assertThat(status, hasEntry(is(fiveCentsOfEuro()), is(0)));

    }

    /**
     * Helper class to generate a list of allowed coins
     */
    private Inventory<Coin> generate() {
        return InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        twoEuros(),
                        oneEuro(),
                        fiftyCentsOfEuro(),
                        twentyCentsOfEuro(),
                        Coins.tenCentsOfEuro(),
                        Coins.fiveCentsOfEuro()
                ));

    }

}
