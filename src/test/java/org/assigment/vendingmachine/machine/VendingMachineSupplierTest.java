package org.assigment.vendingmachine.machine;

import org.assigment.vendingmachine.change.ChangeMachineFactory;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.Coins;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.InventoryFactory;
import org.assigment.vendingmachine.product.Product;
import org.junit.Test;

import java.util.Map;

import static java.util.Arrays.asList;
import static org.assigment.vendingmachine.coin.Coins.*;
import static org.assigment.vendingmachine.product.Products.*;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Validates supplier operations in vending machine
 */
public class VendingMachineSupplierTest {

    @Test
    public void resetAnEmptyMachine() {
        // Setup
        VendingMachine machine = create();

        // Exercise
        machine.reset();

        // Verify
        Map<Product, Integer> productStatus = machine.inventory().status();
        assertThat(productStatus, hasEntry(is(coke()), is(0)));
        assertThat(productStatus, hasEntry(is(pepsi()), is(0)));
        assertThat(productStatus, hasEntry(is(water()), is(0)));

        Map<Coin, Integer> coinStatus = machine.changeMachine().status();
        assertThat(coinStatus, hasEntry(is(twoEuros()), is(0)));
        assertThat(coinStatus, hasEntry(is(oneEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(fiftyCentsOfEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(twentyCentsOfEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(tenCentsOfEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(fiveCentsOfEuro()), is(0)));

    }

    @Test
    public void loadAndReset() {
        VendingMachine machine = create();
        machine.changeMachine().add(oneEuro());
        machine.changeMachine().add(oneEuro());
        machine.changeMachine().add(oneEuro());

        machine.inventory().add(water());
        machine.inventory().add(water());
        machine.inventory().add(water());

        // Verify load
        assertThat(machine.inventory().isAvailable(water()), is(true));

        // Exercise
        machine.reset();

        // Verify
        Map<Product, Integer> productStatus = machine.inventory().status();
        assertThat(productStatus, hasEntry(is(coke()), is(0)));
        assertThat(productStatus, hasEntry(is(pepsi()), is(0)));
        assertThat(productStatus, hasEntry(is(water()), is(0)));

        Map<Coin, Integer> coinStatus = machine.changeMachine().status();
        assertThat(coinStatus, hasEntry(is(twoEuros()), is(0)));
        assertThat(coinStatus, hasEntry(is(oneEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(fiftyCentsOfEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(twentyCentsOfEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(tenCentsOfEuro()), is(0)));
        assertThat(coinStatus, hasEntry(is(fiveCentsOfEuro()), is(0)));

    }

    /**
     * Helper to creates a Vending Machine
     */
    private VendingMachine create() {
        // Setup
        Inventory<Coin> coinInventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Coins.twoEuros(),
                        Coins.oneEuro(),
                        Coins.fiftyCentsOfEuro(),
                        Coins.twentyCentsOfEuro(),
                        Coins.tenCentsOfEuro(),
                        Coins.fiveCentsOfEuro()
                ));

        Inventory<Product> productInventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        coke(),
                        pepsi(),
                        water()
                ));

        VendingMachine vendingMachine = VendingMachineFactory.create(
                VendingMachineFactory.Type.SIMPLE, productInventory,
                ChangeMachineFactory.create(ChangeMachineFactory.Type.SIMPLE, coinInventory));

        return vendingMachine;
    }

}
