package org.assigment.vendingmachine.machine.internal;

import org.assigment.vendingmachine.change.ChangeMachine;
import org.assigment.vendingmachine.change.exception.InsufficientChangeException;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.exception.InvalidCoinException;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.exception.SoldOutException;
import org.assigment.vendingmachine.machine.Delivery;
import org.assigment.vendingmachine.machine.VendingMachine;
import org.assigment.vendingmachine.machine.exception.AmountNotSatisfiedException;
import org.assigment.vendingmachine.product.Product;
import org.assigment.vendingmachine.product.Products;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.assigment.vendingmachine.coin.Coins.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Validates {@link SimpleVendingMachine}
 */
public class SimpleVendingMachineTest {

    @Test
    public void cancelWithAmount0() {
        // Setup
        VendingMachine machine = new SimpleVendingMachine(null, null);

        // Exercise
        Optional<Delivery> canceled = machine.cancel();

        // Verify
        assertThat(canceled.isPresent(), is(false));

    }

    @Test
    public void insertWithoutSelectAProductShouldBeGivenBackInCancel() {
        // Setup
        Inventory<Product> inventory = mock(Inventory.class);
        ChangeMachine changeMachine = mock(ChangeMachine.class);
        VendingMachine machine = new SimpleVendingMachine(inventory, changeMachine);

        when(changeMachine.change(any())).thenAnswer((o) ->
                asList(oneEuro(),
                        twentyCentsOfEuro(),
                        twentyCentsOfEuro(),
                        tenCentsOfEuro(),
                        fiveCentsOfEuro())
        );

        machine.insert(oneEuro());
        machine.insert(twentyCentsOfEuro());
        machine.insert(twentyCentsOfEuro());
        machine.insert(tenCentsOfEuro());
        machine.insert(fiftyCentsOfEuro());

        // Exercise
        Optional<Delivery> canceled = machine.cancel();

        // Verify
        verify(changeMachine, times(1)).add(oneEuro());
        verify(changeMachine, times(2)).add(twentyCentsOfEuro());
        verify(changeMachine, times(1)).add(tenCentsOfEuro());
        verify(changeMachine, times(1)).add(fiftyCentsOfEuro());

        assertThat(canceled.isPresent(), is(true));
        List<Coin> coins = canceled.get().coins();

        assertThat(coins, hasSize(5));

        Map<Coin, Integer> counter = coins.stream().collect(Collectors.groupingBy(coin -> coin, Collectors.summingInt((a) -> 1)));
        assertThat(counter, hasEntry(is(oneEuro()), is(1)));
        assertThat(counter, hasEntry(is(twentyCentsOfEuro()), is(2)));
        assertThat(counter, hasEntry(is(tenCentsOfEuro()), is(1)));
        assertThat(counter, hasEntry(is(fiveCentsOfEuro()), is(1)));

    }

    @Test
    public void insertAnInvalidCoin() {

        // Setup
        Inventory<Product> inventory = mock(Inventory.class);
        ChangeMachine changeMachine = mock(ChangeMachine.class);
        VendingMachine machine = new SimpleVendingMachine(inventory, changeMachine);

        Mockito.doThrow(InvalidCoinException.class).when(changeMachine).add(any());

        // Exercise
        Coin coin = new Coin("0.01€", new BigDecimal("0.01"));
        Optional<Delivery> inserted = machine.insert(coin);

        // Verify
        assertThat(inserted.isPresent(), is(true));
        assertThat(inserted.get().coins(), hasSize(1));
        assertThat(inserted.get().coins(), hasItem(coin));

    }

    @Test(expected = SoldOutException.class)
    public void selectedASoldOutProduct() {

        // Setup
        Inventory<Product> inventory = mock(Inventory.class);
        ChangeMachine changeMachine = mock(ChangeMachine.class);
        VendingMachine machine = new SimpleVendingMachine(inventory, changeMachine);

        when(inventory.isAvailable(any())).thenReturn(Boolean.FALSE);

        // Exercise
        machine.select(Products.water());

    }

    @Test(expected = AmountNotSatisfiedException.class)
    public void selectedAProductWithEnoughCoins() {

        // Setup
        Product water = Products.water();
        Inventory<Product> inventory = mock(Inventory.class);
        ChangeMachine changeMachine = mock(ChangeMachine.class);
        VendingMachine machine = new SimpleVendingMachine(inventory, changeMachine);

        when(inventory.isAvailable(any())).thenReturn(Boolean.TRUE);
        when(inventory.peek(any())).thenReturn(water);

        // Exercise
        machine.insert(fiftyCentsOfEuro());
        machine.select(water);

    }

    @Test(expected = InsufficientChangeException.class)
    public void selectedAProductAnChangeMachineHasNoChange() {

        // Setup
        Product water = Products.water();
        Inventory<Product> inventory = mock(Inventory.class);
        ChangeMachine changeMachine = mock(ChangeMachine.class);
        VendingMachine machine = new SimpleVendingMachine(inventory, changeMachine);

        when(inventory.isAvailable(any())).thenReturn(Boolean.TRUE);
        when(inventory.peek(any())).thenReturn(water);
        when(changeMachine.isChangeAvailable(any())).thenReturn(Boolean.FALSE);

        // Exercise
        machine.insert(fiftyCentsOfEuro());
        machine.insert(fiftyCentsOfEuro());
        machine.select(water);

    }

    @Test
    public void selectedAProduct() {

        // Setup
        Product water = Products.water();
        Inventory<Product> inventory = mock(Inventory.class);
        ChangeMachine changeMachine = mock(ChangeMachine.class);
        VendingMachine machine = new SimpleVendingMachine(inventory, changeMachine);

        when(inventory.isAvailable(any())).thenReturn(Boolean.TRUE);
        when(inventory.peek(any())).thenReturn(water);
        when(changeMachine.isChangeAvailable(any())).thenReturn(Boolean.TRUE);
        when(inventory.remove(any())).thenReturn(water);
        when(changeMachine.change(any())).thenAnswer((o) -> asList(tenCentsOfEuro()));

        // Exercise
        machine.insert(fiftyCentsOfEuro());
        machine.insert(fiftyCentsOfEuro());
        Delivery delivered = machine.select(water);

        // Verify
        assertThat(delivered.product(), is(water));
        assertThat(delivered.coins(), hasSize(1));
        assertThat(delivered.coins(), hasItem(tenCentsOfEuro()));

    }

}
