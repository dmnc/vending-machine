package org.assigment.vendingmachine.machine;

import org.assigment.vendingmachine.change.ChangeMachineFactory;
import org.assigment.vendingmachine.change.exception.InsufficientChangeException;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.Coins;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.InventoryFactory;
import org.assigment.vendingmachine.inventory.exception.SoldOutException;
import org.assigment.vendingmachine.machine.exception.AmountNotSatisfiedException;
import org.assigment.vendingmachine.machine.internal.SimpleVendingMachine;
import org.assigment.vendingmachine.product.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;
import static org.assigment.vendingmachine.coin.Coins.*;
import static org.assigment.vendingmachine.product.Products.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Validates {@link VendingMachine} using {@link SimpleVendingMachine}
 * <p>
 * TODO we should use BDD to test (Cucumber)
 */
public class VendingMachineTest {

    private VendingMachine vendingMachine;

    @Test
    public void insertExactAmountAndSelectAnAvailableProduct() {

        // Setup
        Integer availableCokes = vendingMachine.inventory().status().get(coke());
        Integer available1Euro = vendingMachine.changeMachine().status().get(oneEuro());
        Integer available50Cents = vendingMachine.changeMachine().status().get(fiftyCentsOfEuro());

        // Exercise
        vendingMachine.insert(oneEuro());
        vendingMachine.insert(fiftyCentsOfEuro());

        Delivery select = vendingMachine.select(coke());

        // Verify
        assertThat(select.product(), is(coke()));
        assertThat(select.coins(), is(empty()));

        // Verify change machine status
        Map<Coin, Integer> coinStatus = vendingMachine.changeMachine().status();
        assertThat(coinStatus, hasEntry(is(oneEuro()), is(available1Euro + 1)));
        assertThat(coinStatus, hasEntry(is(fiftyCentsOfEuro()), is(available50Cents + 1)));

        // Verify inventory status
        Map<Product, Integer> productStatus = vendingMachine.inventory().status();
        assertThat(productStatus, hasEntry(is(coke()), is(availableCokes - 1)));

    }

    @Test
    public void insert15CentsExtraAndSelectAnAvailableProduct() {

        // Setup
        Integer availableCokes = vendingMachine.inventory().status().get(coke());
        Integer available2Euro = vendingMachine.changeMachine().status().get(twoEuros());
        Integer available1Euro = vendingMachine.changeMachine().status().get(oneEuro());
        Integer available50Cents = vendingMachine.changeMachine().status().get(fiftyCentsOfEuro());
        Integer available10Cents = vendingMachine.changeMachine().status().get(tenCentsOfEuro());
        Integer available5Cents = vendingMachine.changeMachine().status().get(fiveCentsOfEuro());

        // Exercise
        vendingMachine.insert(oneEuro());
        vendingMachine.insert(fiftyCentsOfEuro());
        vendingMachine.insert(fiveCentsOfEuro());
        vendingMachine.insert(fiveCentsOfEuro());
        vendingMachine.insert(fiveCentsOfEuro());

        Delivery select = vendingMachine.select(coke());

        // Verify
        assertThat(select.product(), is(coke()));
        assertThat(select.coins(), is(not(empty())));
        assertThat(select.coins(), hasSize(2));
        assertThat(select.coins(), hasItem(tenCentsOfEuro()));
        assertThat(select.coins(), hasItem(fiveCentsOfEuro()));

        // Verify change machine status
        Map<Coin, Integer> coinStatus = vendingMachine.changeMachine().status();
        assertThat(coinStatus, hasEntry(is(twoEuros()), is(available2Euro)));
        assertThat(coinStatus, hasEntry(is(oneEuro()), is(available1Euro + 1)));
        assertThat(coinStatus, hasEntry(is(fiftyCentsOfEuro()), is(available50Cents + 1)));
        assertThat(coinStatus, hasEntry(is(tenCentsOfEuro()), is(available10Cents - 1)));
        assertThat(coinStatus, hasEntry(is(fiveCentsOfEuro()), is(available5Cents + 2)));

        // Verify inventory status
        Map<Product, Integer> productStatus = vendingMachine.inventory().status();
        assertThat(productStatus, hasEntry(is(coke()), is(availableCokes - 1)));

    }

    @Test(expected = AmountNotSatisfiedException.class)
    public void tryToSelectWithSomeCoins() {

        // Exercise
        vendingMachine.insert(oneEuro());

        vendingMachine.select(coke());

        // Verify ... check expected

    }

    @Test(expected = SoldOutException.class)
    public void tryToSelectASoldOutProduct() {

        IntStream.range(0, 5).forEach(i -> {

            // Exercise 1 - 5
            vendingMachine.insert(oneEuro());
            vendingMachine.insert(fiftyCentsOfEuro());

            Delivery oneDelivery = vendingMachine.select(coke());

            // Verify 1 - 4
            assertThat(oneDelivery.product(), is(coke()));
            assertThat(oneDelivery.coins(), is(empty()));

        });

        // Verify ... check expected

    }

    @Test(expected = InsufficientChangeException.class)
    public void tryToSelectAProductAndThereIsInsufficientChange() {

        IntStream.range(0, 5).forEach(i -> {

            // Exercise 1 - 5
            vendingMachine.insert(oneEuro());
            vendingMachine.insert(fiftyCentsOfEuro());

            Delivery delivered = vendingMachine.select(pepsi());

            // Verify 1 - 4
            assertThat(delivered.product(), is(pepsi()));
            assertThat(delivered.coins(), hasSize(1));
            assertThat(delivered.coins(), hasItem(fiveCentsOfEuro()));

        });

        // Verify ... check expected

    }

    @Test
    public void tryToRefundWithoutInsertAnyCoin() {

        Optional<Delivery> cancel = vendingMachine.cancel();

        assertThat(cancel.isPresent(), is(false));

    }

    @Test
    public void refund() {

        // Setup
        Integer available2Euro = vendingMachine.changeMachine().status().get(twoEuros());
        Integer available1Euro = vendingMachine.changeMachine().status().get(oneEuro());
        Integer available50Cents = vendingMachine.changeMachine().status().get(fiftyCentsOfEuro());
        Integer available10Cents = vendingMachine.changeMachine().status().get(tenCentsOfEuro());
        Integer available5Cents = vendingMachine.changeMachine().status().get(fiveCentsOfEuro());

        // Exercise
        vendingMachine.insert(oneEuro());
        vendingMachine.insert(fiveCentsOfEuro());
        vendingMachine.insert(fiveCentsOfEuro());

        Optional<Delivery> maybe = vendingMachine.cancel();

        // Verify
        assertThat(maybe.isPresent(), is(true));
        Delivery refunded = maybe.get();
        assertThat(refunded.product(), is(nullValue()));
        assertThat(refunded.coins(), hasSize(2));
        assertThat(refunded.coins(), hasItem(oneEuro()));
        assertThat(refunded.coins(), hasItem(tenCentsOfEuro()));

        // Verify change machine status
        Map<Coin, Integer> coinStatus = vendingMachine.changeMachine().status();
        assertThat(coinStatus, hasEntry(is(twoEuros()), is(available2Euro)));
        assertThat(coinStatus, hasEntry(is(oneEuro()), is(available1Euro)));
        assertThat(coinStatus, hasEntry(is(fiftyCentsOfEuro()), is(available50Cents)));
        assertThat(coinStatus, hasEntry(is(tenCentsOfEuro()), is(available10Cents - 1)));
        assertThat(coinStatus, hasEntry(is(fiveCentsOfEuro()), is(available5Cents + 2)));

    }

    /**
     * Helper to creates a fresh Vending Machine for each test
     */
    @Before
    public void init() {
        // Setup
        Inventory<Coin> coinInventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Coins.twoEuros(),
                        Coins.oneEuro(),
                        Coins.fiftyCentsOfEuro(),
                        Coins.twentyCentsOfEuro(),
                        Coins.tenCentsOfEuro(),
                        Coins.fiveCentsOfEuro()
                ));

        Inventory<Product> productInventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        coke(),
                        pepsi(),
                        water()
                ));

        vendingMachine = VendingMachineFactory.create(
                VendingMachineFactory.Type.SIMPLE, productInventory,
                ChangeMachineFactory.create(ChangeMachineFactory.Type.SIMPLE, coinInventory));

        // load products
        IntStream.range(0, 4).forEach(i -> vendingMachine.inventory().add(water()));
        IntStream.range(0, 8).forEach(i -> vendingMachine.inventory().add(pepsi()));
        IntStream.range(0, 4).forEach(i -> vendingMachine.inventory().add(coke()));

        // load coins
        vendingMachine.changeMachine().add(twoEuros());
        vendingMachine.changeMachine().add(oneEuro());
        IntStream.range(0, 4).forEach(i -> vendingMachine.changeMachine().add(fiftyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> vendingMachine.changeMachine().add(twentyCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> vendingMachine.changeMachine().add(Coins.tenCentsOfEuro()));
        IntStream.range(0, 4).forEach(i -> vendingMachine.changeMachine().add(Coins.fiveCentsOfEuro()));

    }

}
