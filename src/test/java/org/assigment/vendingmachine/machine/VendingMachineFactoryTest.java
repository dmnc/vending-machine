package org.assigment.vendingmachine.machine;

import org.assigment.vendingmachine.change.ChangeMachine;
import org.assigment.vendingmachine.change.ChangeMachineFactory;
import org.assigment.vendingmachine.coin.Coin;
import org.assigment.vendingmachine.coin.Coins;
import org.assigment.vendingmachine.inventory.Inventory;
import org.assigment.vendingmachine.inventory.InventoryFactory;
import org.assigment.vendingmachine.machine.internal.SimpleVendingMachine;
import org.assigment.vendingmachine.product.Product;
import org.assigment.vendingmachine.product.Products;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * Validates {@link VendingMachineFactory}
 */
public class VendingMachineFactoryTest {

    @Test
    public void simpleType() {
        // Setup
        Inventory<Coin> coinInventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Coins.twoEuros(),
                        Coins.oneEuro(),
                        Coins.fiftyCentsOfEuro(),
                        Coins.twentyCentsOfEuro(),
                        Coins.tenCentsOfEuro(),
                        Coins.fiveCentsOfEuro()
                ));

        ChangeMachine changeMachine = ChangeMachineFactory.create(ChangeMachineFactory.Type.SIMPLE, coinInventory);

        Inventory<Product> inventory = InventoryFactory.create(InventoryFactory.Type.SIMPLE,
                asList(
                        Products.coke(),
                        Products.pepsi(),
                        Products.water()
                ));

        // Exercise
        VendingMachine vendingMachine = VendingMachineFactory.create(VendingMachineFactory.Type.SIMPLE, inventory, changeMachine);

        // Verify
        assertThat(vendingMachine, instanceOf(SimpleVendingMachine.class));

    }

}
